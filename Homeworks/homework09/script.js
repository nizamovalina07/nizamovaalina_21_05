'use strict'

fetch ('https://reqres.in/api/users?per_page=12')
	.then((response) => {
		return response.json()
	})
	.then((body) => {

		console.log('-----------');
		console.log('Пункт №1:')
		console.log('-----------');

		body?.data.forEach((item) => {
			console.log(item);
		});

		console.log('-----------');
		console.log('Пункт №2:')
		console.log('-----------');

		body?.data.forEach((item) => {
			console.log(item?.last_name);
		});

		console.log('-----------');
		console.log('Пункт №3:')
		console.log('-----------');

		const findLastName = body?.data.filter((item) => {
			return item?.last_name[0] === 'F';
		});
		console.log(findLastName);
		
		console.log('-----------');
		console.log('Пункт №4:')
		console.log('-----------');
		
		const nameUser = body?.data.reduce((accumulator, item) => {
			return accumulator + item.first_name + ' ' + item.last_name + ', ';
			}, 'Наша база содержит данные следующих пользователей: ');
		console.log(nameUser);
	

		console.log('-----------');
		console.log('Пункт №5:')
		console.log('-----------');
		
		const arrKey = [body?.data];
			for (let ob of arrKey) {
			const arrKeySplice = ob.splice(11, 1);
			console.log(Object.keys(arrKeySplice[0]));
		};
	});
