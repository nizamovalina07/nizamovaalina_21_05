'use strict';

const submitBtn = document.querySelector("#submit")

const applyError = (wrapper) => {
    wrapper.classList.add("error")
}

const removeError = (wrapper) => {
    wrapper.classList.remove("error")
}

const emptyFieldText = "Поле обязательно для заполнения"
const weakPass = "Пароль должен содержать как минимум 8 символов"
const invalidEmail = "Email невалидный"

const displayWarning = (warningElem, text) => {
    warningElem.innerHTML = text
    warningElem.style.display = "block"
}
const hideWarning = (warningElem) => {
    warningElem.style.display = "none"
}

function validateEmail(email) {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


submitBtn.addEventListener('click', (event) => {
    const emailInput = document.querySelector("#email")
    const emailWarning = document.querySelector("#warning_empty_email")
    const emailWrapper = document.querySelector(".wrapper-form__email-wrapper")


    const passInput = document.querySelector("#pass")
    const passWarning = document.querySelector("#warning_empty_pass")
    const passWrapper = document.querySelector(".wrapper-form__pass-wrapper")

    const checkbox = document.querySelector("#checkbox")
    const checkboxSquare = document.querySelector(".wrapper-form__checkbox-square")
    const checkboxWarning = document.querySelector("#warning_empty_checkbox")


    let hasError = false;
    if (emailInput.value === "") {
        applyError(emailWrapper)
        displayWarning(emailWarning, emptyFieldText)
        hasError = true
    } else if (!validateEmail(emailInput.value)) {
        applyError(emailWrapper)
        displayWarning(emailWarning, invalidEmail)
        hasError = true
    } else {
        removeError(emailWrapper)
        hideWarning(emailWarning)
    }

    if (passInput.value === "") {
        applyError(passWrapper)
        displayWarning(passWarning, emptyFieldText)
        hasError = true
    } else if (passInput.value.length < 8) {
        applyError(passWrapper)
        displayWarning(passWarning, weakPass)
        hasError = true
    } else {
        removeError(passWrapper)
        hideWarning(passWarning)
    }


    if (!checkbox.checked) {
        applyError(checkboxSquare)
        displayWarning(checkboxWarning, emptyFieldText)
        hasError = true
    } else {
        removeError(checkboxSquare)
        hideWarning(checkboxWarning)
    }

    if (hasError) {
        console.log("form contain errors")
    } else {
        console.log({"email": emailInput.value, "pass": passInput.value})
    }

    event.preventDefault()
})
